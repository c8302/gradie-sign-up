# Gradie Sign Up Page

This is a challenge of [Codewell](https://www.codewell.cc/challenges/gradie-sign-up-page--608ac420650dff001599e8ec), and I used to learn about tailwind and your powers.

## Demo

[Click here](https://c8302.gitlab.io/gradie-sign-up/) to see page made by me.
